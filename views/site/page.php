<?php
$this->title = $model['title'];
$this->registerMetaTag(['name' => 'keywords', 'content' => $model['metakey']]);
$this->registerMetaTag(['name' => 'description', 'content' => $model['metadiscription']]);
?>
<div class="name_page"><?= $this->title ?></div>

<?= $model['text'] ?>