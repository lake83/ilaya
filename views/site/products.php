<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\PjaxAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Filters;
use app\models\Categories;

$this->title = 'Продукция';
$get_category = Yii::$app->request->getQueryParam('category');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['site/products']];
if (!empty($get_category))
{
    $filters = Categories::filtersList($get_category);
    $this->params['breadcrumbs'][] = $categories[$get_category];
}
elseif (Yii::$app->request->cookies->has('category'))
{
    $get_category = Yii::$app->request->cookies->getValue('category');
    $filters = Categories::filtersList($get_category);
    $this->params['breadcrumbs'][] = $categories[$get_category];
}
else
{
    $get_category = null;
    $this->params['breadcrumbs'][] = 'ВСЕ';
} 

PjaxAsset::register($this);
$this->registerJs(<<<JS
function category(id){
    $.pjax({
        url: $('this').attr('href'),
        container: '#main',
        fragment: '#main',
        data: {category: id},
        scrollTo: false,
        push:false,
        replace:false,
        timeout:1000
   });
}
JS
, yii\web\View::POS_END);
?>
<div class="name_page"><?= $this->title ?></div>

<div class="main_center">
				<div class="lin"></div>
				<div class="main_prod">
					<div class="left_prod">
						<?php foreach($categories as $key => $category): ?>     
                        <div class="left_bar">
							<a href="<?=Url::current()?>" onclick="js:category(<?=$key?>);return false;"><?=$category?></a>
						</div>       
                        <?php endforeach; ?>
					</div>
					<div class="right_prod">
					<div class="sele">
                    <?php 
                     echo Html::beginForm(Url::current(), 'POST', ['id'=>'filter-form']);
                     
                     if (isset($filters) && is_array($filters))
                     {
                         foreach($filters as $filter)
                         {
                             $name = Yii::$app->params['filters_id'][$filter];
                             echo Html::dropDownList($name, isset($_GET[$name]) ? $_GET[$name] : '', 
                                  Filters::selectedFiltersList($get_category, $filter), 
                                  ['onchange' => "js:$.pjax({
                                      url: $('#filter-form').attr('action'),
                                      container: '#main',
                                      fragment: '#main',
                                      data: {".$name.": this.value},
                                      scrollTo: false,
                                      push:false,
                                      replace:false,
                                      timeout:1000
                                  })", 'prompt' => Yii::$app->params['filters'][$filter], 'id' => $name, 'class' => 'field']);
                         } 
                     }
                     echo Html::dropDownList('price', isset($_GET['price']) ? $_GET['price'] : '', 
                          ['DESC' => 'по убыванию', 'ASC' => 'по возростанию'], 
                          ['onchange' => "js:$.pjax({
                                      url: $('#filter-form').attr('action'),
                                      container: '#main',
                                      fragment: '#main',
                                      data: {price: this.value},
                                      scrollTo: false,
                                      push:false,
                                      replace:false,
                                      timeout:1000
                                  })", 'prompt' => 'Цена', 'id' => 'price', 'class' => 'field']);
                          
                     echo Html::endForm();
                    ?>
					</div>
                    <div class="clear"></div>
                    
				<?php Pjax::begin(['options' => ['id'=>'products']]);
                    echo ListView::widget([
                        'dataProvider' => $products,
                        'itemView' => '_product',
                        'pager' => [
                            'nextPageLabel' => false,
                            'prevPageLabel' => false
                         ],
                        'layout' => '{items}<div class="clear"></div><div class="news_pager">{pager}</div>'
                    ]);
                    Pjax::end();                    
                ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>