<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
$form = ActiveForm::begin(['id' => 'call-form', 'class' => 'form']);
echo $form->field($model, 'name')->textInput(['class' => 'input', 'placeholder' => 'Ваше Имя'])->label(false);
echo $form->field($model, 'email')->textInput(['class' => 'input', 'placeholder' => 'Email'])->label(false);
echo $form->field($model, 'phone')->textInput()->label(false)->widget(\yii\widgets\MaskedInput::className(), [
         'options' => [
             'class' => 'input',
             'placeholder' => 'Телефон'
         ],
        'mask' => '999-999-9999'
]);
if ($model->scenario == 'call')
    echo $form->field($model, 'time')->textInput(['class' => 'input', 'placeholder' => 'Удобное время для звонка'])->label(false);
echo $form->field($model, 'product')->hiddenInput()->label(false);
?>
<div class="form-group">
<?= Html::submitButton('Отправить', ['class' => 'submit']); ?>
</div>
<?php ActiveForm::end(); ?>