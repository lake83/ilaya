<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Контакты';
?>
    <div class="name_page"><?= Html::encode($this->title) ?></div>
    
    <div class="main_center">
				<div class="cont_info">
					<div class="cont_left_line"></div>
					<div class="cont_right_line"></div>
					<div class="cont_center_title">Свяжитесь с нами уже сегодня!</div>
					<!--<div class="cont_text">
						Альтернативное объяснение предполагает, что формация деформирует оз, что в конце концов приведет к полному разрушению хребта под действием собственного веса. Замерзание, с зачастую загипсованными породами, стягивает ийолит-уртит. Литосфера, как теперь известно, старица своеобразна. 
					</div>-->
				</div>
				<div class="cont_main_form">
					<div class="cont_form_left">
						<div class="cont_form_adres">
							Адрес: г. Киев, ул. Кирилловская<br> (Фрунзе), 102
						</div>
						<div class="cont_form_tel">
							044-465-70-88 <br> 067-158-41-28
						</div>
						<div class="cont_form_mail">
							info@ilaya-west.com
						</div>
					</div>
					<div class="cont_form_right">
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

             <div class="senks">Спасибо, что связались с нами. Мы ответим Вам как можно скорее.</div>

            <?php else: ?>
			<?php $form = ActiveForm::begin(['id' => 'contact-form', 'class' => 'form']); ?>
                <?= $form->field($model, 'name')->textInput(['class' => 'input', 'size' => 40, 'placeholder' => 'Ваше Имя'])->label(false) ?>
                <?= $form->field($model, 'email')->textInput(['class' => 'input', 'size' => 40, 'placeholder' => 'Email'])->label(false) ?>
                <?= $form->field($model, 'body')->textArea(['class' => 'input','rows' => 3, 'cols' => 40, 'placeholder' => 'Сообщение'])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'submit', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
            <?php endif; ?>
					</div>
				</div>
			</div>
        <div class="karta">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2538.91763917256!2d30.479712793112718!3d50.479876973224705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cde70140c059%3A0x7dc2b61af3da1f46!2z0LLRg9C7LiDQmtC40YDQuNC70ZbQstGB0YzQutCwLCAxMDIsINCa0LjRl9Cy!5e0!3m2!1suk!2sua!4v1434624820633" style="width:100%" height="500" frameborder="0" style="border:0"></iframe>
		</div>