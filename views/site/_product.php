<?php 
use yii\helpers\Url;
use app\widgets\ImagesHelper;
?>
<a class="pad" href="<?= Url::to(['/site/product', 'id' => $model->slug]) ?>" data-pjax="0">
    <div class="item<?=Yii::$app->controller->action->id == 'product' ? 2 : 3?>">
	    <div class="product_a">
		    <div class="in_product
            <?php 
            if ($model->action == 1)
                echo ' action';
            elseif ($model->popular == 1)
                echo ' popular';
            elseif ($model->new == 1)
                echo ' new';
            ?>"></div> 
            <div class="product_a_img specials" <?php if (!empty($model->image)): ?>style="background-image: url(<?php $images = new ImagesHelper(); echo $images->render_asTag($model->image, 207, 207,true)?>);"<?php endif; ?>></div>
			<div class="product_a_name2">
				<?=$model->title?>
			</div>
			<div class="product_a_price">
				<div class="cena_prod"><?=Yii::$app->formatter->asCurrency($model->price, $model->currency)?></div> <span class="cena_prod2"><?=Yii::$app->params['units'][$model->unit]?></span>
			</div>
		</div>
	</div>
</a>