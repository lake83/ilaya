<?php
$this->title = 'Прайс';
$this->registerMetaTag(['name' => 'keywords', 'content' => '']);
$this->registerMetaTag(['name' => 'description', 'content' => '']);
?>
<div class="name_page"><?= $this->title ?></div>
<div class="main_center">
				<div class="main_glob">
					<?php $i = 0; foreach($model as $price): ?>
                    <div class="main_price<?= $i == 3 ? ' mar_top mar_left' : ($i == 4 ? ' mar_top' : '') ?>">
						<div class="price">
							<div class="price_comp"><img src="/images/upload/<?=$price['image']?>" alt="<?=$price['title']?>"/></div>
							<div class="price_name"><?=$price['title']?></div>
							<?php if (!empty($price['file'])): ?>
                            <a target="_blank" href="<?=Yii::getAlias('@web/files/').$price['file']?>"><div class="gjsm">Посмотреть</div></a>
                            <a download="" href="<?=Yii::getAlias('@web/files/').$price['file']?>"><div class="price_sk">Скачать прайс-лист</div></a>
                            <?php endif; ?>
						</div>
					</div>
                    <?php $i == 4 ? $i=0 : $i++; endforeach; ?>
					<div class="clear"></div>
				</div>
			</div>