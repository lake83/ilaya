<?php 
use yii\helpers\Url;
?>
<div class="main_news">
	<div class="news_set">
		<div class="news_img">
			<img src="<?= $model->getThumbFileUrl('image', 'thumb') ?>" alt="<?= $model->title ?>"/>
		</div>
		<div class="news_info">
			<div class="news_date">
				<?= Yii::$app->formatter->asDate($model->created_at, 'long') ?>
			</div>
			<div class="news_title">
				<a title="<?= $model->title ?>" href="<?= Url::to(['/site/article', 'id' => $model->slug]) ?>"><?= $model->title ?></a>
			</div>
			<div class="news_text">
				<?= $model->preview_text ?>
			</div>
		</div>	
	</div>
</div>