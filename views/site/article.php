<?php
$this->title = $model['title'];

$this->registerMetaTag(['name' => 'keywords', 'content' => $model['metakey']]);
$this->registerMetaTag(['name' => 'description', 'content' => $model['metadiscription']]);
?>
<div class="name_page"><?= $this->title ?></div>
<?php
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['site/news']];
$this->params['breadcrumbs'][] = $this->title;	
?>
<div class="main_center"><?= $model['text'] ?></div>