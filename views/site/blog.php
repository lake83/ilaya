<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
$this->title = 'Новости';
$this->registerMetaTag(['name' => 'keywords', 'content' => '']);
$this->registerMetaTag(['name' => 'description', 'content' => '']);
?>
<div class="name_page"><?= Html::encode($this->title) ?></div>
    <div class="main_center">
    <?php Pjax::begin(['options' => ['id'=>'news']]);
    echo ListView::widget([
            'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'pager' => [
                    'nextPageLabel' => false,
                    'prevPageLabel' => false
                ],
                'layout' => '{items}<div class="clearfix"></div><div class="news_pager">{pager}</div>'
        ]);
        Pjax::end();                    
    ?>
    </div>