<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

if (!empty($slider)){
    $this->registerJsFile(YII_DEBUG ? 'js/owl.carousel.js' : 'js/owl.carousel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(YII_DEBUG ? 'css/carousel.css' : 'css/carousel.min.css');
}
if (!empty($projects)){
    $this->registerJsFile('js/jquery.bxslider.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(YII_DEBUG ? 'css/jquery.bxslider.css' : 'css/jquery.bxslider.min.css');   
}
$this->registerJsFile(YII_DEBUG ? 'js/scripts.js' : 'js/scripts.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<section class="t_sect">
			<div class="main_center">
				<div class="left_t_blok">
					<div class="left_t_title">
						Выберите интересующий Вас вид черепицы
					</div>
					<div id="wrapper" class="tab1">
                                                
						<?php $i = 1; foreach($tabs as $key => $tab): ?>
                        <a class="tab<?=$i?>">
						<span class="img_ce ce<?=$i?>">
							
						</span></a>
                        <div class="tab<?=$i?> left_t_text">
							<?=$tabs[$key]['text']?> 
						</div>                        
                        <?php $i++; endforeach; ?>
                                                
					</div>
					<div class="left_t_home">
						
					</div>
				</div>
				<div class="right_t_blok">
		
				</div>
		
			</div>
		</section>
          
        <?php if (!empty($slider)): ?>
        <section class="cvcv">
			<div class="main_center">
				<div id="wrapper1" class="tab11">
					<a class="tab11">Акции</a>
					<a class="tab22">Популярные товары</a>
					<a class="tab33">Новинки</a>
							
					<?php foreach($slider as $key => $slides): ?>
                    <span class="tab<?=$key.$key?>">
                    <div class="owl-carousel">
                     <?php foreach($slides as $slide): ?>
                        <div class="item">
							<a class="pad" href="<?=Url::to(['/site/product', 'id' => $slide['slug']])?>">
                                <div class="product_a">
									<div class="product_a_img" <?php if (!empty($slide['image'])): ?>style="background-image: url(<?=$slide['image']?>);"<?php endif; ?>>
									</div>
									
									<div class="prode">
									</div>
									<div class="product_a_name">
										<?=$slide['title']?>
									</div>
									<div class="product_a_price">
										<div><?=Yii::$app->formatter->asCurrency($slide['price'], $slide['currency'])?></div> <?=Yii::$app->params['units'][$slide['unit']]?>
									</div>
                                </div>    
                             </a>	
						</div>
                    <?php endforeach; ?>
                    </div>
                    </span>
                    <?php endforeach; ?>
				    
			    </div>
            </div>
		</section>
        <?php endif; ?>
        
        <?=$services['text']?>
        
        <?php if (!empty($projects)): ?>
        <div id="projects" class="name_page_bl2">
			<span>Наши Проекты</span>
		</div>
		
		<div class="slider4">
        
        <?php foreach($projects as $project): ?>
            <div class="slide" style="background-image: url(/images/projects/<?=$project['image']?>);">
                <div class="top_prod">
                    <span class="s_ti"><?=$project['title']?></span>
                    <span class="s_ti2"><?=$project['description']?></span>
                    <span class="s_ti3">
                        <a href="#popup" onclick="js:popup_content('<?=$project['alias']?>')" class="btn">Подробнее</a>
                    </span>
                </div>
            </div>
        <?php endforeach; ?>
			
		</div>
        <?php endif; ?>
        
        <a href="#projects" class="overlay" id="popup"></a>
		<div id="popupForm">
		    <a href="#projects" class="close">X</a>
			<div class="main_center">
			    <div class="name_page33">Наши Проекты</div>
                <div class="main_nas">
                    <div id="popup_content"></div>
                </div>
			</div>		
		</div>
        
        <div class="od">
			<div class="main_center">
				<div class="od_left">
					<div class="od_left_od">ОТЗЫВЫ</div>
					<div class="od_left_te">Отзывы наших<br/> клиентов</div>
					
				</div>
                <?php Pjax::begin(['options' => ['id'=>'reviews']]);
                    echo ListView::widget([
                        'dataProvider' => $reviews,
                        'itemView' => '_review',
                        'pager' => [
                            'nextPageLabel' => false,
                            'prevPageLabel' => false
                         ],
                        'layout' => '{items}<div class="clear"></div><div class="rewiews_pager">{pager}</div>'
                    ]);
                    Pjax::end();                    
                ?>
			</div>
				<div class="clear"></div>
		</div>