<?php
use yii\helpers\Url;
use himiklab\colorbox\Colorbox;
use app\widgets\ImagesHelper;

$this->title = $model['title'];
$this->params['breadcrumbs'][] = ['label' => 'Продукция', 'url' => ['site/products']];
$this->params['breadcrumbs'][] = ['label' => $model['category']['title'], 'url' => ['site/products']];
$this->params['breadcrumbs'][] = $this->title;

if (!empty($similar)){
    $this->registerJsFile('/js/owl.carousel.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile('/css/carousel.css');
    $this->registerJs("
        $('.owl-carousel').owlCarousel({
	loop:true,
	margin:10,
	nav:true,
    singleItem:true,    
	responsive:{
		320:{
			items:1
		},
        360:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:4
		}
	}
})", \yii\web\View::POS_END);
}
$this->registerJs("
function popup_content(type){
    $.ajax({
       type: 'POST',
       url: '".Url::to(['/site/product-request'])."',
       data: {'type': type},
       success: function(data) {
           $('.name_page33').text(type == 'call' ? 'Заказать обратный звонок' : 'Уточнить скидку');
           $('#popup_content').html(data);
           $('#productform-product').val($('.active').text());
       }
    });
}
", \yii\web\View::POS_END);
?>
<div class="name_page">Продукция</div>

<?php if (Yii::$app->session->hasFlash('productFormSubmitted')): ?>
<div class="senks">Спасибо, что связались с нами. Мы ответим Вам как можно скорее.</div>
<?php endif; ?>

<div class="main_center">
				<div class="main_prod">
					<div class="left_prod">
						<?php foreach($categories as $key => $category): ?>     
                        <div class="left_bar">
							<a href="<?=Url::to(['site/product', 'category' => $key])?>"><?=$category?></a>
						</div>       
                        <?php endforeach; ?>
					</div>
					<div class="right_prod">
                        <div class="prod_images">
                            <div class="in_product
                            <?php 
                            if ($model['action'] == 1)
                                echo ' action';
                            elseif ($model['popular'] == 1)
                                echo ' popular';
                            elseif ($model['new'] == 1)
                                echo ' new';
                            ?>"></div>                       
                            <div class="img_prod" <?php if (!empty($model['image'])): ?>style="background-image: url('/images/products/<?=$model['image'][0]?>');"<?php endif; ?>></div>
                            <div class="clear"></div>
        <?php 
            $images = new ImagesHelper();
            
            Colorbox::widget([
                'targets' => [
                    '.colorbox' => [
                        'maxWidth' => 800,
                        'maxHeight' => 600,
                        'opacity' => 0.7
                    ],
                ],
                'coreStyle' => 1
            ]);
            
            if (!empty($model['image']))
            {
                foreach($model['image'] as $photo) : ?>
                    <a rel="gallery" class="colorbox" href="/images/products/<?= $photo ?>">
                        <div style="padding:1px;border:0" class="thumbnail">
                            <?= $images->render_asDiv($photo, 70, 70, true) ?>
                        </div>
                    </a>
                <?php endforeach;
            }
        ?>
                        </div>
						<div class="news_title22"><?=$model['title']?></div>
                        <div class="cena"><span class="cena_prod"><?=Yii::$app->formatter->asCurrency($model['price'], $model['currency'])?></span> <span class="cena_prod2"><?=Yii::$app->params['units'][$model['unit']]?></span></div>
                        <a href="#popup" onclick="js:popup_content('clarify')" class="botton product_btn" style="background-color: #94C359;">Уточнить скидку</a>
                        <a href="#popup" onclick="js:popup_content('call')" class="botton product_btn">Заказать обратный звонок</a>
        <a href="#product" class="overlay" id="popup"></a>
		<div id="popupForm">
		    <a href="#product" class="close">X</a>
			<div class="main_center">
			    <div class="name_page33"></div>
                <div class="main_nas">
                    <div id="popup_content"></div>
                </div>
			</div>		
		</div>
						<div class="text_prod"><?=$model['description']?></div>
                        <div class="clear"></div>
                        <br /><hr />
						<div class="news_title22"><?=$model['title']?></div>
                        <?php if (!empty($model['file'])): ?>
                        <?php foreach($model['file'] as $one): ?>
						<a download="" href="<?=Yii::getAlias('@web/files/').$one?>">
                            <div class="dok">Техническая Документация</div>
                        </a>
                        <?php endforeach; ?>
                        <?php endif; ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>
<?php if (!empty($similar) || !empty($related)): ?>
<div class="name_page_bl"><span>Похожие товары</span></div>

    <div class="main_center">
	    <?php if (!empty($similar))
            echo $this->render('_slider', ['data' => $similar]); 
        ?>
        <div class="clear"></div>
                
        <?php if (!empty($related)): ?>
        
		<div class="left_t_title mar">
			С этим тваром также покупают
		</div>
		<div class="p_tov">	
			<?= $this->render('_slider', ['data' => $related]); ?>
			<div class="clear"></div>
		</div>
        
    <?php endif; ?>
	</div>
<?php endif; ?>