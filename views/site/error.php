<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="name_page"><?= Html::encode($this->title) ?></div>

<div class="main_center">
    <?= nl2br(Html::encode($message)) ?>
</div>
