<?php
use yii\helpers\Url;	
?>
<div class="owl-carousel">
    <?php foreach($data as $slide): ?>
    <div class="item">
		<a class="pad" href="<?=Url::to(['/site/product', 'id' => $slide['slug']])?>">
            <div class="product_a">
                <div class="in_product
                <?php 
                if ($slide['action'] == 1)
                    echo ' action';
                elseif ($slide['popular'] == 1)
                    echo ' popular';
                elseif ($slide['new'] == 1)
                    echo ' new';
                ?>"></div> 
				<div class="product_a_img specials" <?php if (!empty($slide['image'])): ?>style="background-image: url(<?=$slide['image']?>);"<?php endif; ?>></div>
				<div class="prode"></div>
				<div class="product_a_name" style="color: #000;">
					<?=$slide['title']?>
				</div>
				<div class="product_a_price">
				<div><?=Yii::$app->formatter->asCurrency($slide['price'], $slide['currency'])?></div> <?=Yii::$app->params['units'][$slide['unit']]?>
				</div>
            </div>    
        </a>	
	</div>
    <?php endforeach; ?>
</div>