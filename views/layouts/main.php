<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web') ?>/images/favicon.ico" />
</head>
<body>

<?php $this->beginBody() ?>
        <header>
			<div class="top_menu2">
				<div class="main_center">
					<a href="/"><div class="logo2">
						<img src="/images/logo.png" alt="logo"/><span class="s_1">ILAYA - WEST</span><br/><span class="s_2">КРОВЕЛЬНЫЙ  ЦЕНТР</span>
					</div></a>
					<ul  class='el2 top_menu_ul2' onclick="((this.className=='el1')?this.className='el2':this.className='el1')">
	
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'contact'?'activ':''?>"><a href="<?=Url::to(['/site/contact'])?>">КОНТАКТЫ</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'dostavka-i-oplata'?'activ':''?>"><a href="<?=Url::to(['/site/dostavka-i-oplata'])?>">ДОСТАВКА И ОПЛАТА</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'news'?'activ':''?>"><a href="<?=Url::to(['/site/news'])?>">НОВОСТИ</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'price'?'activ':''?>"><a href="<?=Url::to(['/site/price'])?>">ПРАЙС</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'products'?'activ':''?>"><a href="<?=Url::to(['/site/products'])?>">ПРОДУКЦИЯ</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'o-nas'?'activ':''?>"><a href="<?=Url::to(['/site/o-nas'])?>">О НАС</a></li>
						<li class="top_menu_li2 <?=Yii::$app->controller->action->id == 'index'?'activ':''?>"><a href="<?=Url::to(['/site/index'])?>">ГЛАВНАЯ</a></li>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <li class="top_menu_li2"><a href="<?=Url::to(['/admin/site/index'])?>">АДМИНКА</a></li>
                        <?php endif; ?>
					</ul>
				</div>	
			</div>
		</header>
    <section id="main">
        <?= Breadcrumbs::widget([
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              'homeLink'=>false
        ]) ?>
        <?= $content ?>
    </section>

<?php $this->beginContent('@app/views/layouts/footer.php');
      $this->endContent(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
