<?php
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$this->registerJs("
$(function() {
   $(window).scroll(function() {
   if($(this).scrollTop() != 0) {
   $('#toTop').fadeIn();
} else { 
   $('#toTop').fadeOut(); 
} 
});
   $('#toTop').click(function() {
   $('body,html').animate({scrollTop:0},800);
});
});
", yii\web\View::POS_END);
?>
<footer style="background-color: #000;padding-top: 50px;clear: both;">
			<div class="main_center">
				<div class="left_f_blok " >
					<div class="m_mob" onclick="((this.className=='el11')?this.className='el22':this.className='el11')">
						<div class="menu_bot">
							О НАС
						</div>
						<div class="tab1 left_t_text">
								Широчайший выбор спец. аксессуаров и самых разнообразных кровельных материалов. 
                                                 <a href="<?=Url::to(['/site/o-nas'])?>" class="botton">ПОДРОБНЕЕ</a>
						</div>
					</div>
				</div>
				<div class="right_f_blok">
					<div class="m_mob" onclick="((this.className=='el11')?this.className='el22':this.className='el11')">
						<div class="menu_bot">
							ДОСТАВКА И ОПЛАТА
						</div>
						<div class="tab1 left_t_text">
								Доставка выбранных у нас кровельных материалов в любую точку Украины даже в нерабочие часы и выходные дни.
						</div>
						<a href="<?=Url::to(['/site/dostavka-i-oplata'])?>" class="botton">ПОДРОБНЕЕ</a>
					</div>
				</div>
				<div class="center_f_blok">
					<div class="m_left">
						<div class="m_mob" onclick="((this.className=='el11')?this.className='el22':this.className='el11')">
							<div class="menu_bot">
								РАЗДЕЛЫ
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/index'])?>">ГЛАВНАЯ</a>
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/products'])?>">ПРОДУКЦИЯ</a>
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/price'])?>">ПРАЙС</a>
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/news'])?>">НОВОСТИ</a>
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/dostavka-i-oplata'])?>">ДОСТАВКА И ОПЛАТА</a>
							</div>
							<div class="razdel">
								<a href="<?=Url::to(['/site/o-nas'])?>">О НАС</a>
							</div>
						</div>
					</div>
					<div class="m_right">
						<div class="m_mob" onclick="((this.className=='el11')?this.className='el22':this.className='el11')">
							<div class="menu_bot">
								КОНТАКТЫ
							</div>
							<div class="adrec left_t_text">
								Адрес: г. Киев, ул. Кирилловская (Фрунзе), 102
							</div>
							<div class="nomer left_t_text">
								044-465-70-88<br />067-158-41-28
							</div>
							<div class="mail left_t_text">
								info@ilaya-west.com
							</div>
							<div class="img_seti"><a rel="nofollow" href="https://plus.google.com/100245820252958438351/posts"><img src="/images/G.png" alt="img"/></a><a rel="nofollow" href="https://www.facebook.com/pages/ilaya-west/971018226298657?skip_nax_wizard=true"><img src="/images/FB.png" alt="img"/></div></a>
						<div style="color:white;"><p>Сайт разработан: <br><a style="color:white; font-weight:bold;" href="http://www.push-k.com.ua/">PUSH-K Solutions</a></p></div>
                                                  </div>	
					</div>
					<div class="clear"></div>
				</div>
			</div>
                 <div id="toTop"><img src="/images/up.png" alt="Вверх"/></div>
		</footer>