<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web') ?>/images/favicon.ico" />
</head>
<body>

<?php $this->beginBody() ?>
        <section class="header_sect">
			<header class="header">
				<div class="main_center">
					<a href="/"><div class="logo">
						<img src="images/logo.png" alt="logo"><span class="s_1"><?=Yii::$app->name?></span><br/><span class="s_2">КРОВЕЛЬНЫЙ  ЦЕНТР</span>
					</div></a>
					<div class="kontakt">
						<a href="#"><div class="set_1">
						</div></a>
						<a href="#"><div class="set_2">
						</div></a>
						<a href="#"><div class="set_3">
						</div></a>
						<a href="#"><div class="set_4">
						</div></a>
						<div class="tel">
							<span class="ss_1">ПОЗВОНИТЕ НАМ:</span> <span class="ss_2">044 465 70 88</span>
						</div>
					</div>
					<div class="top_menu">
					
					<ul id= "col" class='el233 top_menu_ul233 top_menu_ul' onclick="((this.className=='el133')?this.className='el233':this.className='el133')">
						
							<li class="top_menu_li activ"><a href="<?=Url::to(['/site/index'])?>">ГЛАВНАЯ</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/o-nas'])?>">О НАС</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/products'])?>">ПРОДУКЦИЯ</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/price'])?>">ПРАЙС</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/news'])?>">НОВОСТИ</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/dostavka-i-oplata'])?>">ДОСТАВКА И ОПЛАТА</a></li>
							<li class="top_menu_li"><a href="<?=Url::to(['/site/contact'])?>">КОНТАКТЫ</a></li>
                            <?php if (!Yii::$app->user->isGuest): ?>
                            <li class="top_menu_li"><a href="<?=Url::to(['/admin/site/index'])?>">АДМИНКА</a></li>
                            <?php endif; ?>
						</ul>
						
					</div>
					
				</div>
			</header>
            <div class="main_center">
				<h1 class="h1">ВСЕ ВИДЫ ЧЕРЕПИЦЫ<br/> ПО НИЗКИМ ЦЕНАМ</h1>
				<ul class="top_ul">
					<li class="top_li">КЕРАМИЧЕСКАЯ ЧЕРЕПИЦА</li>
					<li class="top_li">КОМПОЗИТНАЯ ЧЕРЕПИЦА</li>
					<li class="top_li">БИТУМНАЯ ЧЕРЕПИЦА</li>
					<li class="top_li">МЕТАЛЛОЧЕРЕПИЦА</li>
				</ul>
				<div class="cherepica1">
					<div class="che_title">
						КЕРАМИЧЕСКАЯ ЧЕРЕПИЦА
					</div>
					<div class="che_text">
						Это абсолютно экологичный материал...
					</div>
					<div class="che_inform">
						<a href="<?=Url::current(['cat' => 'Цементно-песчаная черепица'])?>">УЗНАТЬ БОЛЬШЕ</a>
					</div>
				</div>
				<div class="cherepica2">
					<div class="che_title">
						КОМПОЗИТНАЯ ЧЕРЕПИЦА
					</div>
					<div class="che_text">
						Это элитный материал, который...
					</div>
					<div class="che_inform">
						<a href="<?=Url::current(['cat' => 'Композитная черепица'])?>">УЗНАТЬ БОЛЬШЕ</a>
					</div>
				</div>
				<div class="cherepica3">
				<div class="che_title">
						БИТУМНАЯ <br>ЧЕРЕПИЦА
					</div>
					<div class="che_text">
						Идеально подходит для обустройства...
					</div>
					<div class="che_inform">
						<a href="<?=Url::current(['cat' => 'Битумная черепица'])?>">УЗНАТЬ БОЛЬШЕ</a>
					</div>
				</div>
			</div>
		</section>
            
            <?= $content ?>
            
<?php $this->beginContent('@app/views/layouts/footer.php');
      $this->endContent(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
