<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Filters;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="сategories-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
    
        <div class="col-lg-6">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'filters')->checkboxList(Yii::$app->params['filters']) ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'brend')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(1), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'color')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(2), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'form')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(3), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'use')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(4), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'size')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(5), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-4">   
            <?= $form->field($model, 'material')->widget(\dosamigos\multiselect\MultiSelect::className(), [
                    'options' => ['multiple'=>'multiple'], // for the actual multiselect
                    'data' => Filters::filterList(6), // data as array
                    'clientOptions' => 
                    [
                        'includeSelectAllOption' => false,
                        'numberDisplayed' => 3,
                        'buttonWidth' => '250px'
                    ], 
            ]) ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'active')->checkbox(['onclick' => 'js:if(!this.checked) alert("При деактивации категории будут сняты с публикации все товары этой категории.")']) ?>
        </div> 
    
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
