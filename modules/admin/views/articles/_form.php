<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metakey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metadiscription')->textInput(['maxlength' => true]) ?>
    <div class="row">
    <div class="col-lg-6">
    <?= $form->field($model, 'image')->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
            'options' => ['accept' => 'image/*'],
            'clientOptions' => [
                'previewFileType' => 'text',
                'browseClass' => 'btn btn-success',
                'browseLabel' => 'Выбрать',
                'showUpload' => false,
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                'removeLabel' => 'Удалить',
                'overwriteInitial' => false,
                'maxFileSize' => 2*1024,
                'maxFileCount' => 1,
                'previewSettings' => ['image' => ['width' => '345px','height' => '220px']],
                'initialPreview' => !empty($model->image) ? '<img style="max-width:345px" src="/images/upload/'.$model->image.'" class="file-preview-image">' : ''
           ]])
    ?>
    </div>
    </div>
    <?= $form->field($model, 'text')->widget(
            TinyMce::className(),
            [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'compressorRoute' => 'articles/tinyMceCompressor',
                'fileManager' => [
                    'class' => TinyMceElFinder::className(),
                    'connectorRoute' => 'articles/connector'
                ]
            ]
        )
    ?>
    <span class="hint">Текст превью отделяется тегом &lt;!--more--&gt;.</span>
    
    <?= $form->field($model, 'news')->checkbox() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
