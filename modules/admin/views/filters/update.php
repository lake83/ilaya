<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Filters */

$this->title = 'Редактирование фильтра: ' . ' ' . $model->title;
?>
<div class="filters-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
