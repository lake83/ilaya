<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Filters */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filters-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    
        <div class="col-lg-6">
    
            <?= $form->field($model, 'filter_id')->dropDownList(Yii::$app->params['filters'], ['prompt'=>'- выбрать -']); ?>
    
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            
        </div>
    
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
