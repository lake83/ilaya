<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Filters;
use app\models\Categories;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin();
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'category_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    Categories::categoryList(),
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return $model->category->title;}
            ],
            [
                'attribute' => 'price',
                'value' => function ($model, $index, $widget) {
                    return Yii::$app->formatter->asCurrency($model->price, $model->currency);}
            ],
            [
                'attribute' => 'brend_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'brend_id',
                    Filters::filterList(1),
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return !empty($model->brend->title) ? $model->brend->title : '';}
            ],
            [
                'attribute' => 'color_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'color_id',
                    Filters::filterList(2),
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return !empty($model->color->title) ? $model->color->title : '';}
            ],
            [
                'attribute' => 'form_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'form_id',
                    Filters::filterList(3),
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return !empty($model->form->title) ? $model->form->title : '';}
            ],
            [
                'attribute' => 'use_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'use_id',
                    Filters::filterList(4),
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return !empty($model->use->title) ? $model->use->title : '';}
            ],
            [
                'attribute' => 'active',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'active',
                    Yii::$app->params['is_active'],
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return $model->active == 1 ? 'Активно' : 'Не активно';}
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'options' => ['width' => '50px']
            ]
        ],
    ]);
    
    Pjax::end(); ?>

</div>
