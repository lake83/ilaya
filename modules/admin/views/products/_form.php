<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use app\models\Filters;
use yii\helpers\Url;
use app\widgets\ImagesHelper;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'category_id')->dropDownList(Categories::categoryList(), ['prompt'=>'- выбрать -']); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6">
            <?php $images = new ImagesHelper(); 
            echo $form->field($model, 'image[]')->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
                    'options' => ['accept' => 'image/*', 'multiple' => 'true'],
                    'clientOptions' => [
                        'previewFileType' => 'text',
                        'browseClass' => 'btn btn-success',
                        'browseLabel' => 'Выбрать',
                        'showUpload' => false,
                        'removeClass' => 'btn btn-danger',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                        'removeLabel' => 'Удалить',
                        'overwriteInitial' => false,
                        'maxFileSize' => 2*1024,
                        'maxFileCount' => $model::MAX_FILES,
                        'previewSettings' => ['image' => ['width' => '90px','height' => '100px']],                        
                        'initialPreview' => $model->isNewRecord ? '' : $images->render_asTag($model->image, 100, 100),
                        'initialPreviewConfig' => $model->isNewRecord ? '' : $images->previewConfig($model->image, 100, Url::to(['images/file_delete']), $model->id),
                        'fileActionSettings' => ['removeTitle' => 'Удалить'],
                        'otherActionButtons' => '<button class="kv-file-edit btn btn-xs btn-default" {dataKey} title="Главная" type="button">
                                                 <i class="glyphicon glyphicon-upload text-info"></i></button>'
                     ]
            ]);
            
            $this->registerJs("
                $('#products-image').on('fileimageloaded', function(event, previewId) {
                    if($('.file-preview-frame').length > ".$model::MAX_FILES.") {
                        return { message: 'Ви не можете загружать больше ".$model::MAX_FILES." файлов.' };
                    }
                });
                $('.kv-file-edit').on('click', function() {
                    var key = $(this).data('key');
                    $.ajax({
                        url: '".Url::to(['images/file_main', 'propos' => $model->id])."',
                        method: 'POST',
                        dataType: 'json',
                        data: {key: key},
                        success: function(data) {
                            if(data.result == 1){
                                $('.kv-fileinput-error').hide();
                                $('.text-success').text(data.message);
                            }else{
                                $('.text-success').text('');
                                $('.kv-fileinput-error').text(data.message).show();
                            }
                        }
                    });
                });
            ");
            ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'description')->widget(
                TinyMce::className(),
                [
                    'options' => ['rows' => 6],
                    'language' => 'ru',
                    'compressorRoute' => 'products/tinyMceCompressor',
                    'fileManager' => [
                        'class' => TinyMceElFinder::className(),
                        'connectorRoute' => 'products/connector'
                    ]
                ])
            ?>
        </div>
        <div class="col-lg-3">
            <?php $options = ['data-show-preview' => 'false', 'multiple' => 'true'];
            if ($model::MAX_FILES - count($model->old_files) <= 0)
                $options = array_merge($options, ['disabled' => 'true']);
            echo $form->field($model, 'file[]')->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
                   'options' => $options,
                   'clientOptions' => [
                       'browseClass' => 'btn btn-success',
                       'browseLabel' => 'Выбрать',
                       'showUpload' => false,
                       'removeClass' => 'btn btn-danger',
                       'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
                       'removeLabel' => 'Удалить',
                       'maxFileSize' => 10*1024,
                       'maxFileCount' => $model::MAX_FILES - count($model->old_files),
                       'fileActionSettings' => ['removeTitle' => 'Удалить']                        
                   ]
            ]); ?>
            <?php if (!empty($model->file) && is_array($model->file)): ?>
                <span class="hint">Загружены файлы:<br /> 
                <?php foreach($model->file as $one) 
                echo $one.Html::a('&times;',['products/del_file', 'id' => $model->id, 'key' => $one],['title' => 'Удалить', 'class' => 'del_file']).'<br />' ?>
                </span>
            <?php endif; ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'similar')->textInput(['maxlength' => true]) ?>
            <span class="hint">ID продуктов через запятую.</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'currency')->dropDownList(Yii::$app->params['currency'], ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'unit')->dropDownList(Yii::$app->params['units'], ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'related')->textInput(['maxlength' => true]) ?>
            <span class="hint">ID продуктов через запятую.</span>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'brend_id')->dropDownList(Filters::filterList(1), ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'color_id')->dropDownList(Filters::filterList(2), ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'form_id')->dropDownList(Filters::filterList(3), ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'use_id')->dropDownList(Filters::filterList(4), ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'size_id')->dropDownList(Filters::filterList(5), ['prompt'=>'- выбрать -']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'material_id')->dropDownList(Filters::filterList(6), ['prompt'=>'- выбрать -']); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'active')->checkbox() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'action')->checkbox() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'popular')->checkbox() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'new')->checkbox() ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
