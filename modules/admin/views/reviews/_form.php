<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
    
        <div class="col-lg-6">
    
            <?= $form->field($model, 'image')->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
                    'options' => ['accept' => 'image/*'],
                    'clientOptions' => [
                        'previewFileType' => 'text',
                        'browseClass' => 'btn btn-success',
                        'browseLabel' => 'Выбрать',
                        'showUpload' => false,
                        'removeClass' => 'btn btn-danger',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                        'removeLabel' => 'Удалить',
                        'overwriteInitial' => false,
                        'maxFileSize' => 2*1024,
                        'maxFileCount' => 1,
                        'previewSettings' => ['image' => ['width' => '80px','height' => '80px']],
                        'initialPreview' => !empty($model->image) ? '<img style="max-width:300px" src="/images/reviews/'.$model->image.'" class="file-preview-image">' : ''
                ]])
            ?>
            
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'text')->textArea(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'active')->checkbox() ?>
            
        </div>
    
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
