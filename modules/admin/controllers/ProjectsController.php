<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AdminController;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\ProjectsSearch'
            ],
            'create' => [
                'class' => 'app\modules\admin\controllers\actions\Create',
                'model' => 'app\models\Projects',
                'success' => 'Проект успешно создан.'
            ],
            'update' => [
                'class' => 'app\modules\admin\controllers\actions\Update',
                'model' => 'app\models\Projects'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Projects'
            ]
        ];
    }      
}
