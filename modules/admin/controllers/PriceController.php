<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AdminController;

/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\PriceSearch'
            ],
            'create' => [
                'class' => 'app\modules\admin\controllers\actions\Create',
                'model' => 'app\models\Price',
                'success' => 'Прайс успешно создан.'
            ],
            'update' => [
                'class' => 'app\modules\admin\controllers\actions\Update',
                'model' => 'app\models\Price'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Price'
            ]
        ];
    }      
}
