<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AdminController;
use app\models\Products;
use app\widgets\ImagesHelper;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Json;
use zxbodya\yii2\tinymce\TinyMceCompressorAction;
use zxbodya\yii2\elfinder\ConnectorAction;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\ProductsSearch'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Products'
            ],
            'tinyMceCompressor' => [
                'class' => TinyMceCompressorAction::className(),
            ],
            'connector' =>[         
                'class' => ConnectorAction::className(),         
                'settings' => [        
                    'root' => $_SERVER['DOCUMENT_ROOT'] . '/web/images/upload/',                    
                    'URL' => 'http://'.$_SERVER['HTTP_HOST'] . '/web/images/upload/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                ]                    
            ]
        ];
    }
    
    public function actionCreate()
    {
        $model = new Products;
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) 
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $images = new ImagesHelper();
                $model->image = $images->upload('', UploadedFile::getInstances($model, 'image'));
                $model->save();
                
                Yii::$app->session->setFlash('success', 'Продукт успешно создан.');
                return $this->redirect(['index']);
            }
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $model = Products::findOne($id);
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) 
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model == null) 
            throw new NotFoundHttpException('Страница не найдена.');
            
        $photos = (!empty($model->image)) ? $model->image : '';
        $old_files = (!empty($model->file)) ? $model->file : '';
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $images = new ImagesHelper();
                $model->image = $images->upload($photos, UploadedFile::getInstances($model, 'image'));
                $model->save();
                
                Yii::$app->session->setFlash('success', 'Изменения сохранены.');
                return $this->redirect(['index']);
            }
        } 
        else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDel_file($id, $key)
    {
        $model = Products::find()->select(['file'])->where(['id' => $id])->asArray()->one();
        $file = Json::decode($model['file']);
        unset($file[array_search($key, $file)]);
        
        $filepath = Yii::getAlias('@webroot').'/files/';
                
        if(is_file($filepath.$key))
            unlink($filepath.$key);
            
        if (Yii::$app->db->createCommand()->update('products', ['file' => Json::encode($file)], 'id = :id', [':id' => $id])->execute())
            $this->redirect(['update', 'id' => $id]);  
    }     
}
