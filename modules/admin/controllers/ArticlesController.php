<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\AdminController;
use zxbodya\yii2\tinymce\TinyMceCompressorAction;
use zxbodya\yii2\elfinder\ConnectorAction;

/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\ArticlesSearch'
            ],
            'create' => [
                'class' => 'app\modules\admin\controllers\actions\Create',
                'model' => 'app\models\Articles',
                'success' => 'Статья успешно создана.'
            ],
            'update' => [
                'class' => 'app\modules\admin\controllers\actions\Update',
                'model' => 'app\models\Articles'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Articles'
            ],
            'tinyMceCompressor' => [
                'class' => TinyMceCompressorAction::className(),
            ],
            'connector' =>[         
                'class' => ConnectorAction::className(),         
                'settings' => [        
                    'root' => $_SERVER['DOCUMENT_ROOT'] . '/web/images/upload/',                    
                    'URL' => 'http://'.$_SERVER['HTTP_HOST'] . '/web/images/upload/',         
                    'rootAlias' => 'Home',         
                    'mimeDetect' => 'none'         
                ]                    
            ]
        ];
    }
}
