<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AdminController;

/**
 * ReviewsController implements the CRUD actions for Reviews model.
 */
class ReviewsController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\ReviewsSearch'
            ],
            'create' => [
                'class' => 'app\modules\admin\controllers\actions\Create',
                'model' => 'app\models\Reviews',
                'success' => 'Отзыв успешно создан.'
            ],
            'update' => [
                'class' => 'app\modules\admin\controllers\actions\Update',
                'model' => 'app\models\Reviews'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Reviews'
            ]
        ];
    }      
}
