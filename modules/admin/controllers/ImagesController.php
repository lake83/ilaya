<?php
namespace app\modules\admin\controllers;

use Yii;
use app\models\Products;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use app\widgets\ImagesHelper;
use yii\web\NotFoundHttpException;

/**
 * Images controller
 */
class ImagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [$this->action->id],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ]
        ];
    }
    
    public function actionFile_main($propos)
    {
        if(Yii::$app->request->isAjax && isset($_POST['key']))
        {
            $model = $this->loadModel($propos);
            $photos = $model->image;
            Yii::$app->response->format = 'json';
                                    
            if($photos[0] == $_POST['key'])
                return ['result' => 0, 'message' => 'Это изображение уже главное.'];
            else
            {
                $key = array_search($_POST['key'], $photos);
                unset($photos[$key]);
                array_unshift($photos, $_POST['key']);
                $model->image = Json::encode($photos);
            
                if($model->save())
                    return ['result' => 1, 'message' => 'Изображение "'.$_POST['key'].'" установлено как главное.'];
                else
                    return ['result' => 0, 'message' => 'Невозможно сохранить изменения.'];
            }
        }
    }
    
    public function actionFile_delete()
    {
        $id = Yii::$app->request->bodyParams['id'];
        $model = $this->loadModel($id);
        $photos = $model->image;  
        Yii::$app->response->format = 'json';
        
        $transaction = Yii::$app->db->beginTransaction();
        try 
        {
            $del_file = Yii::$app->request->bodyParams['key'];
            if(!empty($del_file)) 
            {
                $key = array_search($del_file, $photos);
                unset($photos[$key]);
                $model->image = !empty($photos) ? Json::encode(array_values($photos)) : '';
                
                if($model->save())
                {
                    $filepath = Yii::getAlias('@webroot/images/products').'/'.$del_file.'*';
                
                    foreach (glob($filepath) as $file) 
                    {
                        if(is_file($file))
                            unlink($file);
                    }
                }
                $images = new ImagesHelper();
                
                $items = [
                    'initialPreview' => $images->render_asTag($photos, 100, 100),
                    'initialPreviewConfig' => $images->previewConfig($photos, 100, Url::to(['images/file_delete']), $id),
                    'append' => true
                ];
                
                $transaction->commit();
                
                return $items;
            }
            else
                return ['result' => 0, 'message' => 'Невозможно сохранить изменения.'];
        }
        catch(Exception $e) 
        {
            $transaction->rollback();
        }
    }
    
    private function loadModel($id)
    {
        $model = Products::findOne($id);
        
        if ($model == null) 
            throw new NotFoundHttpException('Страница не найдена.');
        else 
            return $model;
    }
}