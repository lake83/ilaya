<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\AdminController;

/**
 * FiltersController implements the CRUD actions for Filters model.
 */
class FiltersController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\admin\controllers\actions\Index',
                'search' => 'app\models\FiltersSearch'
            ],
            'create' => [
                'class' => 'app\modules\admin\controllers\actions\Create',
                'model' => 'app\models\Filters',
                'success' => 'Фильтр успешно создан.'
            ],
            'update' => [
                'class' => 'app\modules\admin\controllers\actions\Update',
                'model' => 'app\models\Filters'
            ],
            'delete' => [
                'class' => 'app\modules\admin\controllers\actions\Delete',
                'model' => 'app\models\Filters'
            ]
        ];
    }
}