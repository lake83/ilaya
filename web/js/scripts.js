$('#wrapper a').click(function() {
	if ($(this).attr('class') != $('#wrapper').attr('class') ) {
		$('#wrapper').attr('class',$(this).attr('class'));
	}
});
$('#wrapper1 a').click(function() {
	if ($(this).attr('class') != $('#wrapper1').attr('class') ) {
		$('#wrapper1').attr('class',$(this).attr('class'));
	}
});
$('.owl-carousel').owlCarousel({
	loop:true,
	margin:10,
	nav:true,
    singleItem:true,    
	responsive:{
		320:{
			items:1
		},
        360:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:4
		}
	}
});
$('.slider4').bxSlider({
	slideWidth: 335,
	minSlides: 2,
	maxSlides: 3,
	moveSlides: 1
});
function popup_content(alias){
    $.ajax({
       type: 'POST',
       async: false,
       url: '/',
       data: {'alias': alias},
       success: function(data) {
           $('#popup_content').html(data);
       }
    });
}