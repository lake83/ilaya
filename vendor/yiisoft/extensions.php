<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  '2amigos/yii2-transliterator-helper' => 
  array (
    'name' => '2amigos/yii2-transliterator-helper',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@dosamigos/transliterator' => $vendorDir . '/2amigos/yii2-transliterator-helper',
    ),
  ),
  'zxbodya/yii2-tinymce' => 
  array (
    'name' => 'zxbodya/yii2-tinymce',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@zxbodya/yii2/tinymce' => $vendorDir . '/zxbodya/yii2-tinymce',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'zxbodya/yii2-elfinder' => 
  array (
    'name' => 'zxbodya/yii2-elfinder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@zxbodya/yii2/elfinder' => $vendorDir . '/zxbodya/yii2-elfinder',
    ),
  ),
  'zelenin/yii2-slug-behavior' => 
  array (
    'name' => 'zelenin/yii2-slug-behavior',
    'version' => '0.5.0.0',
    'alias' => 
    array (
      '@Zelenin/yii/behaviors' => $vendorDir . '/zelenin/yii2-slug-behavior',
    ),
  ),
  '2amigos/yii2-file-input-widget' => 
  array (
    'name' => '2amigos/yii2-file-input-widget',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@dosamigos/fileinput' => $vendorDir . '/2amigos/yii2-file-input-widget/src',
    ),
  ),
  'yii-dream-team/yii2-upload-behavior' => 
  array (
    'name' => 'yii-dream-team/yii2-upload-behavior',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@yiidreamteam/upload' => $vendorDir . '/yii-dream-team/yii2-upload-behavior/src',
    ),
  ),
);
