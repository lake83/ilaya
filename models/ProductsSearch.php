<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `app\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'brend_id', 'color_id', 'form_id', 'use_id', 'size_id', 'material_id', 'action', 'popular', 'new', 'active'], 'integer'],
            [['title', 'slug', 'description', 'image', 'currency', 'unit', 'file', 'related', 'similar'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        $query = ($query == null) ? Products::find() : $query;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['category_id'=>SORT_DESC]],        
            'pagination' => array('pageSize' => Yii::$app->controller->action->id == 'products' ? 20 : 10),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'unit' => $this->unit,
            'brend_id' => $this->brend_id,
            'color_id' => $this->color_id,
            'form_id' => $this->form_id,
            'use_id' => $this->use_id,
            'size_id' => $this->size_id,
            'material_id' => $this->material_id,
            'action' => $this->action,
            'popular' => $this->popular,
            'new' => $this->new,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'related', $this->related])
            ->andFilterWhere(['like', 'similar', $this->similar]);

        return $dataProvider;
    }
}
