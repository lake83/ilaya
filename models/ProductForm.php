<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ProductForm is the model behind the product form.
 */
class ProductForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $time;
    public $product;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['time', 'phone', 'name', 'email'], 'required', 'on' => 'call'],
            ['email', 'email'],
            [['phone', 'product'], 'safe']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'time' => 'Удобное время для звонка'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->scenario == 'call' ? 'Заказ обратного звонка с сайта '.Yii::$app->name : 'Уточнить скидку на сайте '.Yii::$app->name)
                ->setTextBody($this->scenario == 'call' ? 
                  'Пользователь '.$this->name.' с E-mail: '.$this->email.' заказал обратный звонок на телефон '.$this->phone.' в '.$this->time.' по товару '.$this->product.'.' :
                  'Пользователь '.$this->name.' с E-mail: '.$this->email.(!empty($this->phone) ? ' и телефоном '.$this->phone : '').' хочет уточнить скидку по товару '.$this->product.'.')
                ->send();

            return true;
        } else {
            return false;
        }
    }
}