<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property string $title
 * @property string $metakey
 * @property string $metadiscription
 * @property string $slug
 * @property string $text
 * @property string $image
 * @property integer $news
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Articles extends \yii\db\ActiveRecord
{
    public $preview_text;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                 'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                 'attribute' => 'image',
                 'thumbs' => [
                    'thumb' => ['width' => 345, 'height' => 220],
                 ],
                 //'filePath' => '@webroot/images/reviews/[[basename]]',
                 //'fileUrl' => '/images/reviews/[[basename]]',
                 'thumbPath' => '@webroot/images/upload/[[basename]]',
                 'thumbUrl' => '/images/upload/[[basename]]',
             ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => true,
                'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
               'transliterateOptions' => 'Russian-Latin/BGN;'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['active', 'news', 'created_at', 'updated_at'], 'integer'],
            [['title', 'metakey', 'metadiscription', 'slug'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, jpg, gif']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'metakey' => 'Metakey',
            'metadiscription' => 'Metadiscription',
            'slug' => 'Псевдоним',
            'text' => 'Текст',
            'image' => 'Изображение превью новости',
            'news' => 'Новость',
            'active' => 'Активно',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->preview_text = strstr($this->text, '<!--more-->', true);
    }
}
