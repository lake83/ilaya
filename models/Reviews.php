<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $image
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                 'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                 'attribute' => 'image',
                 'thumbs' => [
                    'thumb' => ['width' => 80, 'height' => 80],
                 ],
                 //'filePath' => '@webroot/images/reviews/[[basename]]',
                 //'fileUrl' => '/images/reviews/[[basename]]',
                 'thumbPath' => '@webroot/images/reviews/[[basename]]',
                 'thumbUrl' => '/images/reviews/[[basename]]',
             ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at', 'active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, jpg, gif']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'text' => 'Текст',
            'image' => 'Изображение',
            'active' => 'Активно',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано'
        ];
    }
}
