<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Categories;

/**
 * CategoriesSearch represents the model behind the search form about `app\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'filters', 'brend', 'color', 'form', 'use', 'size', 'material'], 'safe'],
            [['active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'filters', $this->filters])
            ->andFilterWhere(['like', 'brend', $this->brend])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'form', $this->form])
            ->andFilterWhere(['like', 'use', $this->use])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'material', $this->material])
            ->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}