<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
use app\widgets\ImagesHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $category_id
 * @property string $image
 * @property string $price
 * @property string $currency
 * @property integer $unit
 * @property string $file
 * @property integer $brend_id
 * @property integer $color_id
 * @property integer $form_id
 * @property integer $use_id
 * @property integer $size_id
 * @property integer $material_id
 * @property integer $action
 * @property integer $popular
 * @property integer $new
 * @property string $related
 * @property string $similar
 * @property integer $active
 *
 * @property Categories $category
 * @property Filters $brend
 * @property Filters $color
 * @property Filters $form
 * @property Filters $use
 */
class Products extends \yii\db\ActiveRecord
{
    const MAX_FILES = 5;
    
    public $old_files = '';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => true,
                'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
               'transliterateOptions' => 'Russian-Latin/BGN;'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'category_id', 'price', 'currency', 'unit', 'brend_id'], 'required'],
            [['description'], 'string'],
            [['category_id', 'brend_id', 'color_id', 'form_id', 'use_id', 'size_id', 'material_id', 'action', 'popular', 'new', 'active'], 'integer'],
            [['color_id', 'form_id', 'use_id', 'size_id', 'material_id'], 'default', 'value' => 0],
            [['price'], 'number'],
            [['title', 'slug', 'related', 'similar'], 'string', 'max' => 255],
            [['related', 'similar'], 'match', 'pattern' => '/^[0-9,]+$/', 'message' => 'Допустимы только цифры и запятая'],
            ['image', 'file', 'extensions' => 'png, jpeg, jpg, gif', 'maxFiles' => self::MAX_FILES, 'skipOnEmpty' => true],
            ['file', 'file', 'extensions' => 'txt, xlsx, xls, csv, doc, docx, pdf', 'maxFiles' => self::MAX_FILES, 'skipOnEmpty' => true],
            [['currency'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'slug' => 'Псевдоним',
            'description' => 'Описание',
            'category_id' => 'Категория',
            'image' => 'Изображение',
            'price' => 'Цена',
            'currency' => 'Валюта',
            'unit' => 'Еденицы измерения',
            'file' => 'Файл',
            'brend_id' => 'Бренд',
            'color_id' => 'Цвет',
            'form_id' => 'Форма',
            'use_id' => 'Применение',
            'size_id' => 'Размер',
            'material_id' => 'Материал',
            'action' => 'Акция',
            'popular' => 'Популярные товары',
            'new' => 'Новинки',
            'related' => 'Рекомендуемые',
            'similar' => 'Похожие товары',
            'active' => 'Активно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrend()
    {
        return $this->hasOne(Filters::className(), ['id' => 'brend_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Filters::className(), ['id' => 'color_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Filters::className(), ['id' => 'form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUse()
    {
        return $this->hasOne(Filters::className(), ['id' => 'use_id']);
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!empty($this->file))
        {
            $this->file = UploadedFile::getInstances($this, 'file');
            if(!file_exists(Yii::getAlias('@webroot/files')))
                mkdir(Yii::getAlias('@webroot/files'), 0777);
                
            foreach ($this->file as $file)
            {
                $title = Yii::$app->getSecurity()->generateRandomString(10) . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot/files') . '/' . $title);
                $files_title[] = $title;
            }
            if(!empty($this->old_files) && isset($files_title))
                $files_res = Json::encode(array_merge($this->old_files, $files_title));
            elseif(isset($files_title))
                $files_res = Json::encode($files_title);
            elseif(!empty($this->old_files))
                $files_res = Json::encode($this->old_files);
            else
                $files_res = '';
            
            $this->file = $files_res;
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if(!empty($this->image))
            $this->image = Json::decode($this->image);
        if(!empty($this->file))
        {
            $this->file = json_decode($this->file, true);
            $this->old_files = $this->file;
        } 
        return parent::afterFind();
    }
    
    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        if(!empty($this->image))
        {
            $images = new ImagesHelper();
            $images->delete($this->image);
        }
        if(!empty($this->file))
        {
            foreach($this->file as $one)
            {
                $filepath = Yii::getAlias('@webroot').'/files/';
                
                if(is_file($filepath.$one))
                    unlink($filepath.$one);
            }
        }
        return parent::afterDelete();
    }
}
