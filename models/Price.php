<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $file
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                 'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                 'attribute' => 'image',
                 'thumbs' => [
                    'thumb' => ['width' => 195, 'height' => 65],
                 ],
                 //'filePath' => '@webroot/images/reviews/[[basename]]',
                 //'fileUrl' => '/images/reviews/[[basename]]',
                 'thumbPath' => '@webroot/images/upload/[[basename]]',
                 'thumbUrl' => '/images/upload/[[basename]]',
             ],
             [
                 'class' => '\yiidreamteam\upload\FileUploadBehavior',
                 'attribute' => 'file',
                 'filePath' => '@webroot/files/[[basename]]',
                 'fileUrl' => '/files/[[basename]]',
             ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpeg, jpg, gif'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'txt, xlsx, xls, csv, doc, docx, pdf']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'file' => 'Файл',
            'active' => 'Активно',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано'
        ];
    }
}
