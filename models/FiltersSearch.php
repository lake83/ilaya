<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Filters;

/**
 * FiltersSearch represents the model behind the search form about `app\models\Filters`.
 */
class FiltersSearch extends Filters
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'filter_id'], 'safe'],
            [['filter_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Filters::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'filter_id' => $this->filter_id
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->orderBy(['filter_id' => SORT_DESC]);

        return $dataProvider;
    }
}