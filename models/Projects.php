<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property string $image
 * @property integer $active
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }
    
    public function behaviors()
    {
        return [
            [
                 'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                 'attribute' => 'image',
                 /*'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                 ],*/
                 'filePath' => '@webroot/images/projects/[[basename]]',
                 'fileUrl' => '/images/projects/[[basename]]',
                 //'thumbPath' => '@webroot/images/[[profile]]_[[pk]].[[extension]]',
                 //'thumbUrl' => '/images/[[profile]]_[[pk]].[[extension]]',
             ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'alias'], 'required'],
            [['description'], 'string'],
            [['active'], 'integer'],
            [['title', 'alias', 'description'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, jpg, gif']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'alias' => 'Псевдоним статьи',
            'description' => 'Описание',
            'image' => 'Изображение',
            'active' => 'Активно',
        ];
    }
}
