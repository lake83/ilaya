<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use app\models\Categories;

/**
 * This is the model class for table "filters".
 *
 * @property integer $id
 * @property string $filter_id
 * @property string $title
 */
class Filters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_id', 'title'], 'required'],
            [['filter_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'filter_id' => 'Фильтр'
        ];
    }
    
    public static function filterList($id)
    {
        $list = ArrayHelper::map(static::find()->where(['filter_id' => $id])->all(), 'id', 'title');
        return !empty($list) ? $list : [0 => 'нет результатов'];
    }
    
    public static function selectedFiltersList($category, $filter)
    {
        if (!is_null($category) && !empty($filter))
        {
            $column = \Yii::$app->params['filters_id'][$filter];
            $filtrs = Categories::find()->select([$column])->where(['id' => $category])->one();
            return ArrayHelper::map(static::find()->where(['in', 'id', $filtrs->$column])->all(), 'id', 'title');
        }
        else
            return [];
    }
}