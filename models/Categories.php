<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use app\models\Products;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $filters
 * @property string $brend
 * @property string $color
 * @property string $form
 * @property string $use
 * @property string $size
 * @property string $material
 * @property integer $active
 */
class Categories extends \yii\db\ActiveRecord
{
    public $is_active;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['filters', 'brend', 'color', 'form', 'use', 'size', 'material'], 'safe'],
            [['active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'filters' => 'Фильтры',
            'brend' => 'Бренды',
            'color' => 'Цвет',
            'form' => 'Форма',
            'use' => 'Применение',
            'size' => 'Размер',
            'material' => 'Материал',
            'active' => 'Активно'
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!empty($this->filters))
            $this->filters = implode(',', $this->filters);
        if (!empty($this->brend))
            $this->brend = implode(',', $this->brend);
        if (!empty($this->color))
            $this->color = implode(',', $this->color);
        if (!empty($this->form))
            $this->form = implode(',', $this->form);
        if (!empty($this->use))
            $this->use = implode(',', $this->use);
        if (!empty($this->size))
            $this->size = implode(',', $this->size);
        if (!empty($this->material))
            $this->material = implode(',', $this->material);
        if (!$this->isNewRecord && $this->is_active != $this->active)
        {
            $is_active =  $this->active == 0 ? 0 : 1;
            Products::updateAll(['active' => $is_active], 'category_id='.$this->id.' AND active=1');
        } 
        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->is_active = $this->active;
        if (!empty($this->filters))
            $this->filters = explode(',', $this->filters);
        if (!empty($this->brend))
            $this->brend = explode(',', $this->brend);
        if (!empty($this->color))
            $this->color = explode(',', $this->color);
        if (!empty($this->form))
            $this->form = explode(',', $this->form);
        if (!empty($this->use))
            $this->use = explode(',', $this->use);
        if (!empty($this->size))
            $this->size = explode(',', $this->size);
        if (!empty($this->material))
            $this->material = explode(',', $this->material);
    }
    
    public static function categoryList()
    {
        return ArrayHelper::map(static::find()->where(['active' => 1])->all(), 'id', 'title');
    }
    
    public static function filtersList($id)
    {
        $category = static::find()->select('filters')->where(['id' => $id])->asArray()->one();
        return explode(',', $category['filters']);
    }
}