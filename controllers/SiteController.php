<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ProductForm;
use app\models\Articles;
use app\models\ArticlesSearch;
use app\models\Categories;
use app\models\Products;
use app\models\ProductsSearch;
use app\models\Projects;
use app\models\Reviews;
use app\models\Price;
use app\models\ReviewsSearch;
use app\models\User;
use yii\web\BadRequestHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\widgets\ImagesHelper;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
    
    public function beforeAction($event)
    {
        if ($this->action->id != 'admin')
            Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = ['css' => []];
        return parent::beforeAction($event);
    }

    public function actionIndex()
    { 
        $this->layout = 'main_page';
        
        $meta = User::find()->select(['main_key', 'main_description'])->where(['status' => User::ROLE_ADMIN])->asArray()->one();
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta['main_key']]);
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta['main_description']]);
        
        if (isset($_GET['cat']))
        {
            $category = Categories::find()->select('id')->where(['title' => $_GET['cat']])->asArray()->one();
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'category',
                'value' => $category['id']
            ]));
            $this->redirect(['site/products']);
        }
        if (Yii::$app->request->isAjax && isset($_POST['alias']))
        {
            $project = Articles::find()->where(['slug' => $_POST['alias']])->asArray()->one();
            $this->isModel($project);
            echo $project['text'];
            Yii::$app->end();
        }
        $tabs = Articles::find()->select(['text'])->where(['LIKE', 'slug', 'tab%', false])->asArray()->all();
        $services = Articles::find()->where(['slug' => 'nashi-uslugi'])->asArray()->one();
        $slider = Products::find()->select(['slug','title','image','price','currency','unit','action','popular','new'])
                                  ->where(['OR', 'action=1', 'popular=1', 'new=1'])->andWhere(['active' => 1])->asArray()->all();
        $reviewsModel = new ReviewsSearch;
        $dataProvider = $reviewsModel->search(Yii::$app->request->queryParams, Reviews::find()->select('name,text,image')->where(['active' => 1]));
        
        foreach($slider as $slide)
        {
           if (!empty($slide['image']))
           {
               $img = Json::decode($slide['image']);
               $images = new ImagesHelper();
               $slide['image'] = $images->render_asTag($img, 207, 207,true);
           }
           else
               $slide['image'] = '';    
               
           if ($slide['action'] == 1)
               $slider_all[1][] = $slide;
           if ($slide['popular'] == 1)
               $slider_all[2][] = $slide;
           if ($slide['new'] == 1)
               $slider_all[3][] = $slide;                     
        }
        $projects = Projects::find()->where(['active' => 1])->asArray()->all();
        
        return $this->render('index', [
                'tabs' => $tabs,
                'services' => $services,
                'slider' => $slider_all,
                'projects' => $projects,
                'reviews' => $dataProvider
            ]);
    }

    public function actionAdmin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/site/index']);
        } else {
            return $this->renderAjax('login', [
                'model' => $model
            ]);
        }
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            
            $admin = User::findOne(['status' => User::ROLE_ADMIN]);
            if (empty($admin) || empty($admin->email)) throw new BadRequestHttpException('Не задан E-mail администратора.');
            
            $model->contact($admin->email);
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model
            ]);
        }
    }

    public function actionNews()
    {
        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('blog', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionArticle($id)
    {
        $model = Articles::find()->where(['slug' => $id])->asArray()->one();
        
        $this->isModel($model);
        
        return $this->render('article', [
            'model' => $model
        ]);
    }
    
    public function actionDostavkaIOplata()
    {
        $model = Articles::find()->where(['slug' => 'dostavka-i-oplata'])->asArray()->one();
        
        $this->isModel($model);
        
        return $this->render('page', [
            'model' => $model
        ]);
    }
    
    public function actionONas()
    {
        $model = Articles::find()->where(['slug' => 'o-nas'])->asArray()->one();
        
        $this->isModel($model);
        
        return $this->render('page', [
            'model' => $model
        ]);
    }
    
    public function actionProducts()
    {
        $where = '`active` = :active';
        $params = [':active' => 1];
        
        if (isset($_GET['category']) && !empty($_GET['category']))
        {
            $where .= ' AND `category_id` = :category';
            $params = array_merge($params,[':category' => $_GET['category']]);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'category',
                'value' => $_GET['category']
            ]));
        }
        elseif(Yii::$app->request->cookies->has('category'))
        {
            $where .= ' AND `category_id` = :category';
            $params = array_merge($params,[':category' => Yii::$app->request->cookies->getValue('category')]);
        }
        foreach(Yii::$app->params['filters_id'] as $filter)
        {
            if (isset($_GET[$filter]) && !empty($_GET[$filter]))
            {
                $where .= ' AND `'.$filter.'_id` = :'.$filter;
                $params = array_merge($params,[':'.$filter => $_GET[$filter]]);
            }
        }
        
        $productsModel = new ProductsSearch;
        $dataProvider = $productsModel->search(Yii::$app->request->queryParams, Products::find()
                        ->select('slug,title,image,price,currency,unit,action,popular,new')
                        ->where($where)
                        ->orderBy(isset($_GET['price']) ? 'price '.$_GET['price'] : 'price ASC')
                        ->addParams($params));
        
        return $this->render('products', [
            'categories' => Categories::categoryList(),
            'products' => $dataProvider
        ]);
    }
    
    public function actionProduct($id = null, $category = null)
    {
        if (!is_null($category))
        {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'category',
                'value' => $category
            ]));
            $this->redirect(['site/products']);
        }
        $model = Products::find()->where(['active' => 1, 'slug' => $id])->with('category')->asArray()->one();
        if (!empty($model['image']))
            $model['image'] = Json::decode($model['image']);
        if (!empty($model['file']))
            $model['file'] = Json::decode($model['file']);
        
        $this->isModel($model);
                        
        return $this->render('product', [
            'categories' => Categories::categoryList(),
            'model' => $model,
            'similar' => $this->productSlider($model['similar']),
            'related' => $this->productSlider($model['related'])
        ]);
    }
    
    public function actionProductRequest()
    {
        $model = new ProductForm();
        if (isset($_POST['type']) && $_POST['type'] == 'call')
             $model->scenario = 'call';
                
        if ($model->load(Yii::$app->request->post())) {
            
            $admin = User::findOne(['status' => User::ROLE_ADMIN]);
            if (empty($admin) || empty($admin->email)) throw new BadRequestHttpException('Не задан E-mail администратора.');
            
            $model->contact($admin->email);
            Yii::$app->session->setFlash('productFormSubmitted');

            $this->redirect(Yii::$app->request->referrer);
        }
        return $this->renderAjax('_popup', [
            'model' => $model
        ]);
    }
    
    private function productSlider($param)
    {
        if (!empty($param))
        {
            $result = Products::find()->select(['slug','title','image','price','currency','unit','action','popular','new'])
                    ->where('`active` = 1 AND id IN('.$param.')')->asArray()->all();
            foreach($result as $key => $slide)
            {
                if (!empty($slide['image']))
                {
                    $img = Json::decode($slide['image']);
                    $images = new ImagesHelper(); 
                    $result[$key]['image'] = $images->render_asTag($img, 207, 207,true);
                }
                else
                    $slide[$key]['image'] = '';                  
            }
        }
        else
            $result = '';
            
        return $result;
    }
    
    public function actionPrice()
    {
        $model = Price::find()->select('title,image,file')->where(['active' => 1])->asArray()->all();
        
        return $this->render('price', [
            'model' => $model
        ]);
    }
    
    private function isModel($model)
    {
        if(!$model)
            throw new BadRequestHttpException('Статья не найдена.');
    }
}